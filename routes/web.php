<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ExcelController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\ModuleController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\EvaluationController;
use App\Http\Controllers\ModuleImageController;
use App\Http\Controllers\AdminStudentController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ProjectImageController;
use App\Http\Controllers\AdminLecturerController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\UserCredentialController;
use App\Http\Controllers\NotificationSeenController;
use App\Http\Controllers\AdminEvaluationFileController;
use App\Http\Controllers\LecturerProjectGuideFileController;
use App\Http\Controllers\LecturerProjectOwnerFileController;
use App\Http\Controllers\GroupLeaveController;
use App\Http\Controllers\NotificationSeenAllController;
use App\Http\Controllers\StudentEvaluationSubmissionController;

Route::get('/login', [AuthController::class, 'create'])->name('login');
Route::post('/login', [AuthController::class, 'store']);

Route::get('/forgot-password', [ForgotPasswordController::class, 'forgotPasswordShow'])->name('password.request');
Route::post('/forgot-password', [ForgotPasswordController::class, 'sentEmail'])->name('password.email');
Route::get('/reset-password/{token}', [ForgotPasswordController::class, 'resetPasswordShow'])->name('password.reset');
Route::post('/reset-password', [ForgotPasswordController::class, 'resetPassword'])->name('password.update');

Route::delete('/logout', [AuthController::class, 'destroy']);

//general routes for authorized users
Route::middleware('auth')->group(function () {
  Route::get('/dashboard', [IndexController::class, 'dashboard']);
  Route::resource('project', ProjectController::class);
  Route::resource('module', ModuleController::class);
  Route::resource('evaluation', EvaluationController::class);
  Route::resource('group', GroupController::class);
  Route::put('group/{group}/leave', GroupLeaveController::class);
  Route::resource('profile/user', ProfileController::class)->only(['show', 'edit', 'update'])->names('profile');
  Route::resource('change-password/user', UserCredentialController::class)->only(['edit', 'update'])->names('change-password');
  Route::resource('notification', NotificationController::class)->only(['index']);
  Route::put('notification/{notification}/seen', NotificationSeenController::class);
  Route::put('notification/seen-all', NotificationSeenAllController::class);
});

//admin route goes here
Route::prefix('admin')
  ->name('admin.')
  ->middleware('role:admin')
  ->group(function () {
    Route::get('/', function () {
      return redirect('/admin/project');
    });
    Route::resource('image/project', ProjectImageController::class)->only(['destroy']);
    Route::post('/image/project/{project}', [ProjectImageController::class, 'store']);

    Route::resource('image/module', ModuleImageController::class)->only(['destroy']);
    Route::post('/image/module/{module}', [ModuleImageController::class, 'store']);

    Route::resource('evaluation.evaluationFile', AdminEvaluationFileController::class)->only(['store', 'destroy']);

    Route::resource('student/user', AdminStudentController::class)->names('student');
    Route::get('/student/upload', [IndexController::class, 'uploadStudent']);
    Route::post('/student/import', [ExcelController::class, 'importStudent']);
    Route::get('/student/export', [ExcelController::class, 'exportStudent']);

    Route::resource('lecturer/user', AdminLecturerController::class)->names('lecturer');
    Route::get('/lecturer/upload', [IndexController::class, 'uploadLecturer']);
    Route::post('/lecturer/import', [ExcelController::class, 'importLecturer']);
    Route::get('/lecturer/export', [ExcelController::class, 'exportLecturer']);
  });

//student route goes here
Route::prefix('student')
  ->name('student.')
  ->middleware('role:student')
  ->group(function () {
    Route::get('/', function () {
      return redirect('/student/evaluation');
    });
    Route::resource('evaluation.evaluationSubmission', StudentEvaluationSubmissionController::class)->only(['store', 'destroy']);
  });

//lecturer route goes here
Route::prefix('lecturer')
  ->name('lecturer.')
  ->middleware('role:lecturer')
  ->group(function () {
    Route::get('/', function () {
      return redirect('/lecturer/evaluation');
    });

    Route::resource('project.projectOwnerFile', LecturerProjectOwnerFileController::class)->only(['store', 'destroy']);
    Route::resource('project.projectGuideFile', LecturerProjectGuideFileController::class)->only(['store', 'destroy']);
  });


//index redirect
Route::get('/', function () {
  $user = Auth::user();
  if ($user) {
    if ($user->role === 'admin') {
      return redirect('/project');
    } else if ($user->role === 'student') {
      return redirect('/dashboard');
    } else if ($user->role === 'lecturer') {
      return redirect('/dashboard');
    }
  } else {
    return redirect('/login');
  }
});

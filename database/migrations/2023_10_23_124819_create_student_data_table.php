<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('student_data', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->enum('programme', ['Bachelor of Science in Computer Science', 'Bachelor of Science in Information Technology', 'Bachelor Of Computer Science (AI Development & Data Science)', 'Bachelor Of Computer Science (Blockchain Development)', 'Bachelor Of Computer Science (Full Stack Development)', 'Bachelor of Digital Media & Development']);
            $table->year('admission_year');
            $table->foreignIdFor(\App\Models\User::class)
                ->constrained('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('student_data');
    }
};

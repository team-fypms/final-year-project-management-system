<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('lecturer_data', function (Blueprint $table) {
            $table->id('id');
            $table->bigInteger('cid')->unique();
            $table->timestamps();

            $table->foreignIdFor(\App\Models\User::class)
                ->constrained('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('lecturer_data');
    }
};

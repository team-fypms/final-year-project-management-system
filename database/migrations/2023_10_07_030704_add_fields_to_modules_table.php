<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('modules', function (Blueprint $table) {
            $table->tinyText('name');
            $table->tinyText('code');
            $table->year('year');
            $table->longText('description');
            $table->string('image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropColumns('modules', [
            'name', 'code', 'year', 'image', 'description'
        ]);

        Storage::disk('public')->deleteDirectory('module_image');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('name');
            $table->longText('description');
            $table->string('image')->nullable();

            $table->foreignIdFor(\App\Models\Module::class, 'module_id')
            ->constrained('modules')->onDelete('cascade');;

            $table->foreignIdFor(\App\Models\User::class, 'owner_id')
            ->constrained('users')->onDelete('cascade');;
            $table->foreignIdFor(\App\Models\User::class, 'guide_id')
            ->constrained('users')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('projects');
    }
};

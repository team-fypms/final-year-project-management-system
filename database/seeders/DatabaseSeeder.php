<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Evaluation;
use App\Models\User;
use App\Models\Group;
use App\Models\Module;
use App\Models\Project;
use App\Models\StudentData;
use App\Models\LecturerData;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    // public function run(): void
    // {
    //     User::factory(1)->create([
    //         'name' => 'Admin Tshering',
    //         'email' => 'admin@gmail.com',
    //         'role' => 'admin',
    //         'password' => 'password'
    //     ]);

    //     User::factory(1)->create([
    //         'name' => 'Lecturer Yonten',
    //         'email' => 'lecturer@gmail.com',
    //         'role' => 'lecturer',
    //         'password' => 'password'
    //     ])->each(function ($user) {
    //         $lecturerData = LecturerData::factory()->make();

    //         $user->lecturerData()->save($lecturerData);
    //     });

    //     User::factory(1)->create([
    //         'name' => 'Dawa Gyeltshen',
    //         'email' => '12200001.gcit@rub.edu.bt',
    //         'role' => 'student',
    //         'password' => 'password'
    //     ])->each(function ($user) {
    //         $studentData = StudentData::factory()->make();

    //         $user->studentData()->save($studentData);
    //     });

    //     User::factory(20)->create(['role' => 'student'])->each(function ($user) {
    //         $studentData = StudentData::factory()->make();

    //         $user->studentData()->save($studentData);
    //     });

    //     User::factory(12)->create(['role' => 'lecturer'])->each(function ($user) {
    //         $lecturerData = LecturerData::factory()->make();

    //         $user->lecturerData()->save($lecturerData);
    //     });

    //     Module::factory(10)->create()
    //         ->each(function ($module) {
    //             $project = Project::factory()->make([
    //                 'owner_id' => 1,
    //                 'guide_id' => 2
    //             ]);

    //             $module->projects()->save($project);
    //         });

    //     Group::factory(10)->create()
    //         ->each(function ($group) {
    //             $students = User::where('role', 'student')->whereNot('id', $group->leader_id)->inRandomOrder()->limit(4)->get()->toArray();
    //             $members = array_map(function ($student) {
    //                 return $student['id'];
    //             }, $students);
    //             array_push($members, $group->leader_id);
    //             $group->members()->attach($members);
    //         });

    //     Evaluation::factory(10)->create()
    //         ->each(function ($evaluation) {
    //             $lecturers = User::where('role', 'lecturer')->inRandomOrder()->limit(4)->get()->toArray();
    //             $evaluators = array_map(function ($student) {
    //                 return $student['id'];
    //             }, $lecturers);
    //             $evaluation->evaluators()->attach($evaluators);
    //         });
    // }

    public function run(): void
    {
        User::factory(1)->create([
            'name' => 'Fypms administrator',
            'email' => 'admin@gmail.com',
            'role' => 'admin',
            'password' => 'password'
        ]);

        User::factory(1)->create([
            'name' => 'Yonten Jamtsho',
            'email' => 'yontenjamtsho.gcit@rub.edu.bt',
            'role' => 'lecturer',
            'password' => 'password'
        ])->each(function ($user) {
            $lecturerData = LecturerData::factory()->make([
                'cid' => 11504001936
            ]);

            $user->lecturerData()->save($lecturerData);
        });

        User::factory(1)->create([
            'name' => 'Tawmo',
            'email' => 'tawmo.gcit@rub.edu.bt',
            'role' => 'lecturer',
            'password' => 'password'
        ])->each(function ($user) {
            $lecturerData = LecturerData::factory()->make([
                'cid' => 10605001742
            ]);

            $user->lecturerData()->save($lecturerData);
        });

        User::factory(1)->create([
            'name' => 'Tashi  Phuntsho',
            'email' => 'tphuntsho.gcit@rub.edu.bt',
            'role' => 'lecturer',
            'password' => 'password'
        ])->each(function ($user) {
            $lecturerData = LecturerData::factory()->make([
                'cid' => 10101004063
            ]);

            $user->lecturerData()->save($lecturerData);
        });

        Module::factory(1)->create([
            'name' => 'Agile Software Enginerring Practice',
            'code' => 'CSF-202',
            'year' => 2023
        ]);

        Module::factory(1)->create([
            'name' => 'Deep Learning',
            'code' => 'CSA-301',
            'year' => 2023
        ]);

        Module::factory(1)->create([
            'name' => 'Design for User Interaction',
            'code' => 'CSC-106',
            'year' => 2023
        ]);

        Module::factory(1)->create([
            'name' => 'Project(PRJ101 & PRJ102)',
            'code' => 'PRJ-101 & PRJ-102',
            'year' => 2023
        ])->each(function ($module) {
            $module->projects()->save(Project::factory()->make([
                'name' => 'Facilities Management system',
                'owner_id' => 4,
                'guide_id' => 4
            ]));

            $module->projects()->save(Project::factory()->make([
                'name' => 'Gate control system',
                'owner_id' => 4,
                'guide_id' => 4
            ]));

            $module->projects()->save(Project::factory()->make([
                'name' => 'Gyalsung Allocation System',
                'owner_id' => 4,
                'guide_id' => 4
            ]));
        });
    }
}

<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Module>
 */
class ModuleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->randomElement(['Mobile App Development', 'Website Development', '1st Year Project', '2nd Year Project', '3rd Year Project', 'Final Year Project']),
            'code' => "PRJ" . fake()->numberBetween(100, 999),
            'year' => fake()->numberBetween(2020, 2023),
            'description' => fake()->realText(),
            'image' => ''
        ];
    }
}

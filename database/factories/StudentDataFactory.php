<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\StudentData>
 */
class StudentDataFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'admission_year' => fake()->numberBetween(2020, 2023),
            'programme' => fake()->randomElement(['Bachelor of Science in Computer Science', 'Bachelor of Science in Information Technology', 'Bachelor Of Computer Science (AI Development & Data Science)', 'Bachelor Of Computer Science (Blockchain Development)', 'Bachelor Of Computer Science (Full Stack Development)', 'Bachelor of Digital Media & Development']),
        ];
    }
}

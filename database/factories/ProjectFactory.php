<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Project>
 */
class ProjectFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->randomElement(['Car parking system', 'Final year project management system', 'Gyalsung Allocation', '10 Million trees planatation system', 'Facilities Management system', 'Gate control system']),
            'description' => fake()->realText(),
            'image' => ''
        ];
    }
}

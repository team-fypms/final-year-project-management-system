<?php

namespace Database\Factories;

use App\Models\Group;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Evaluation>
 */
class EvaluationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $group = Group::inRandomOrder()->limit(1)->get()[0];

        return [
            'name' => 'CA ' . fake()->numberBetween(1, 4) . ' ' . $group->name,
            'description' => fake()->realText(),
            'group_id' => $group->id,
            'start_date_time' => fake()->dateTimeInInterval('+1 days', '+3 days'),
            'end_date_time' => fake()->dateTimeInInterval('+1 week', '+3 days'),
            'rubric' => fake()->name()
        ];
    }
}

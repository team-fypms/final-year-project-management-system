<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Project;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Group>
 */
class GroupFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $project = Project::with('module:id,code')->inRandomOrder()->limit(1)->get()[0];
        $students = User::where('role', 'student')->inRandomOrder()->limit(1)->get();

        return [
            'name' => "Group " . fake()->numberBetween(1, 7) . " " . fake()->randomElement(['CS', 'IT']) . " " . $project->module->code,
            'project_id' => $project->id,
            'leader_id' => $students[0]->id
        ];
    }
}

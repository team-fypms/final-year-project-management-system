import { createApp, h } from 'vue'
import { createInertiaApp } from '@inertiajs/vue3'
import MainLayout from './Layouts/MainLayout.vue'
import FormLayout from './Layouts/FormLayout.vue'
import '../css/app.css'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import {
  faArrowLeft, faFolder, faCaretLeft, faCaretRight, faFile, faTrash, faDownload, faUser, faCircleUser,
  faHelmetSafety, faGraduationCap, faPersonChalkboard, faClock, faRightFromBracket, faPen, faLock, faBell, faCircleCheck,
  faBars, faPeopleGroup, faBarChart, faDiagramProject, faBook, faFileWaveform, faCalendar, faAnglesRight, faHome, faRadio
} from '@fortawesome/free-solid-svg-icons'
import { faFacebook, faXTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons'


/* add icons to the library */
library.add(faArrowLeft, faFolder, faCaretLeft, faCaretRight, faFile, faTrash, faDownload, faUser, faCircleUser, faHelmetSafety,
  faGraduationCap, faPersonChalkboard, faClock, faRightFromBracket, faPen, faLock, faBell, faCircleCheck, faBars, faPeopleGroup,
  faDiagramProject, faBook, faFileWaveform, faCalendar, faAnglesRight, faHome, faRadio
)
library.add(faXTwitter, faFacebook, faInstagram, faBarChart)

createInertiaApp({
  resolve: name => {
    const pages = import.meta.glob('./Pages/**/*.vue', { eager: true })
    let page = pages[`./Pages/${name}.vue`]
    // if (name.startsWith('Admin/')) {
    //   const arr = name.split('/')
    //   if (arr.length > 2) {
    //     if (arr[2].startsWith('Create') || arr[2].startsWith('Upload')) {
    //       page.default.layout = page.default.layout || FormLayout
    //     } else {
    //       page.default.layout = page.default.layout || MainLayout
    //     }
    //   } else {
    //     page.default.layout = page.default.layout || MainLayout
    //   }
    // }
    if (name.startsWith('Student/') || name.startsWith('Lecturer/') || name.startsWith('Admin/')) {
      page.default.layout = page.default.layout || MainLayout
    } else if (name.startsWith('Profile') || name.startsWith('Notification')) {
      page.default.layout = page.default.layout || FormLayout
    } else if (name.startsWith('Project') || name.startsWith('Group') || name.startsWith('Module') || name.startsWith('Evaluation') || name === 'Index/Dashboard'){
      page.default.layout = page.default.layout || MainLayout
    }

    return page
  },
  setup({ el, App, props, plugin }) {
    createApp({ render: () => h(App, props) })
      .use(plugin)
      .component('Icon', FontAwesomeIcon)
      .mount(el)
  },
})
<?php

namespace App\Http\Controllers;

use App\Models\Module;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ModuleImageController extends Controller
{
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, Module $module)
    {
        $validated = $request->validate([
            'image' => 'nullable|image'
        ]);


        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('module_image', 'public');
            $validated['image'] = $path;
            if ($module->image) {
                Storage::disk('public')->delete($module->image);
            }
        } else {
            return redirect()->back()->with('success', 'Nothing was changed!');
        }

        $module->update($validated);

        return redirect()->route('module.show', ['module' => $module->id])
            ->with('success', 'Module image changed!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Module $module)
    {
        if ($module->image) {
            Storage::disk('public')->delete($module->image);
        }
        $module->image = null;
        $module->save();

        return redirect()->route('module.show', ['module' => $module->id])
            ->with('success', 'Module image removed!');
    }
}

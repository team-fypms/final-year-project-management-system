<?php

namespace App\Http\Controllers;

use App\Models\Evaluation;
use Illuminate\Http\Request;
use App\Models\EvaluationSubmission;
use Illuminate\Support\Facades\Storage;
use App\Notifications\EvaluationNotification;

class StudentEvaluationSubmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Evaluation $evaluation, Request $request)
    {
        $now = date("Y-m-d H:i:s");

        // dd($now, $evaluation->start_date_time);
        if ($now < $evaluation->start_date_time) {
            return back()->withErrors(['submissions' => 'You cannot submit now! The evaluation is not open.']);
        } else if ($now > $evaluation->end_date_time) {
            return back()->withErrors(['submissions' => 'You cannot submit! The evaluation is closed.']);
        }

        if ($request->hasFile('submissions')) {
            foreach ($request->file('submissions') as $file) {
                $path = $file->store('evaluations/' . $evaluation->id . '/submissions', 'public');

                $evaluation->submissions()->save(new EvaluationSubmission([
                    'file' => $path,
                    'name' => $file->getClientOriginalName(),
                    'ext' => $file->extension()
                ]));
            }

            $evaluation->evaluators->each(function ($evaluator) use ($request, $evaluation) {
                if ($request->user()->id != $evaluator->id) {
                    $evaluator->notify(new EvaluationNotification($evaluation, $request->user(), 'evaluationSubmissionsAdded'));
                }
            });
            $evaluation->group->members->each(function ($member) use ($request, $evaluation) {
                if ($request->user()->id != $member->id) {
                    $member->notify(new EvaluationNotification($evaluation, $request->user(), 'evaluationSubmissionsAdded'));
                }
            });
        }

        return redirect()->route('evaluation.show', ['evaluation' => $evaluation->id])->with('success', 'Submissions uploaded!');
    }

    /**
     * Display the specified resource.
     */
    public function show(EvaluationSubmission $evaluationSubmission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(EvaluationSubmission $evaluationSubmission)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, EvaluationSubmission $evaluationSubmission)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, Evaluation $evaluation, EvaluationSubmission $evaluationSubmission)
    {
        $now = date("Y-m-d H:i:s");
        if ($now > $evaluation->end_date_time) {
            return back()->withErrors(['submissions' => 'You cannot delete! The evaluation is closed.']);
        }

        Storage::disk('public')->delete($evaluationSubmission->file);
        $evaluationSubmission->delete();

        $evaluation->evaluators->each(function ($evaluator) use ($request, $evaluation) {
            if ($request->user()->id != $evaluator->id) {
                $evaluator->notify(new EvaluationNotification($evaluation, $request->user(), 'evaluationSubmissionsDeleted'));
            }
        });
        $evaluation->group->members->each(function ($member) use ($request, $evaluation) {
            if ($request->user()->id != $member->id) {
                $member->notify(new EvaluationNotification($evaluation, $request->user(), 'evaluationSubmissionsDeleted'));
            }
        });

        return redirect()->back()->with('success', 'Submission was deleted!');
    }
}

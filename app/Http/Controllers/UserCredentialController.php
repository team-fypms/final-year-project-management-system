<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserCredentialController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user)
    {
        $this->authorize('update', $user);

        return inertia('Profile/ChangePassword', [
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user)
    {
        $this->authorize('update', $user);

        $request->validate([
            'oldPassword' => 'required',
            'newPassword' => 'required|string|min:8',
            'confirmPassword' => 'required|same:newPassword'
        ]);

        $result = Auth::attempt([
            'email' => $user->email,
            'password' => $request['oldPassword']
        ], true);

        $password = Hash::make($request['newPassword']);

        if ($result) {
            $user->update(['password' => $password]);

            return redirect()->route('profile.show', ['user' => $user->id])->with('success', 'Password updated!');
        } else {
            return back()->withErrors([
                'oldPassword' => 'The provided credentials do not match our records.',
            ])->onlyInput('oldPassword');
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Group;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Notifications\GroupNotification;

class GroupController extends Controller
{
    function __construct()
    {
        $this->authorizeResource(Group::class, 'group');
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->user()->role == 'admin') {
            $filters = $request->only('name', 'sortBy');

            return inertia('Group/Index', [
                "groups" => Group::with(['project:id,module_id,name', 'project.module:id,year'])
                    ->with('leader:id,name')
                    ->withCount('members')
                    ->filter($filters)
                    ->paginate(8)
                    ->withQueryString(),
                "filters" => $filters
            ]);
        } else if ($request->user()->role == 'student') {
            $filters = $request->only('name', 'sortBy');

            return inertia('Group/Index', [
                "groups" => $request->user()
                    ->memberGroups()
                    ->with(['project:id,module_id,name', 'project.module:id,year'])
                    ->with('leader:id,name')
                    ->withCount('members')
                    ->filter($filters)
                    ->paginate(8)
                    ->withQueryString(),
                "filters" => $filters
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        $filters = $request->only(['memberName']);

        return inertia('Group/Create', [
            "projects" => Project::select('id', 'module_id', 'name')->with('module:id,code,year')->get(),
            'members' => User::findByNameOrEmail($request->memberName, ['student'])->whereNot('id', $request->user()->id)->select('id', 'name', 'email', 'role')->limit(20)->get(),
            'filters' => $filters
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'project' => 'required',
            'members' => 'array|max:10',
        ]);

        $validated['project_id'] = $request['project'];
        $validated['leader_id'] = $request->user()->id;

        $group = Group::create($validated);
        $group->members()->sync($validated['members']);
        $group->members()->syncWithoutDetaching([$validated['leader_id']]);

        $group->members->each(function ($member) use ($request, $group) {
            if ($request->user()->id != $member->id) {
                $member->notify(new GroupNotification($group, $request->user(), 'groupCreated'));
            }
        });

        return redirect()->route('group.index')->with('success', 'Group created!');
    }

    /**
     * Display the specified resource.
     */
    public function show(Group $group)
    {
        $group->load('members', 'project', 'project.module', 'evaluations');

        return inertia('Group/Show', [
            "group" => $group
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, Group $group)
    {
        $group->load('members', 'leader');
        $filters = $request->only(['memberName', 'leaderName']);

        return inertia('Group/Edit', [
            "group" => $group,
            "projects" => Project::select('id', 'module_id', 'name')->with('module:id,code,year')->get(),
            'leaders' => User::findByNameOrEmail($request->leaderName, ['student'])->select('id', 'name', 'email', 'role')->limit(20)->get(),
            'members' => User::findByNameOrEmail($request->memberName, ['student'])->select('id', 'name', 'email', 'role')->limit(20)->get(),
            'filters' => $filters
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Group $group)
    {
        $validated = $request->validate([
            'name' => 'required',
            'project' => 'required',
            'leader' => 'required',
            'members' => 'array|max:10'
        ]);

        $validated['leader_id'] = $request['leader'];
        $validated['project_id'] = $request['project'];

        if ($validated['leader_id'] != $group->leader_id) {
            array_push($validated['members'], $group->leader_id);
            $group->members->each(function ($member) use ($request, $group, $validated) {
                if ($request->user()->id != $member->id && $member->id != $validated['leader_id']) {
                    $member->notify(new GroupNotification($group,  $request->user(), 'groupLeaderChanged'));
                }
            });
            if($request->user()->id != $group->leader_id){
                $group->leader->notify(new GroupNotification($group,  $request->user(), 'groupLeaderRemoved'));
            }
            if($request->user()->id != $validated['leader_id']){
                User::find($validated['leader_id'])->notify(new GroupNotification($group,  $request->user(), 'groupLeaderAdded'));
            }
        }

        $oldMembers = $group->members->map(function ($member) {
            return $member->id;
        })->toArray();
        $group->update($validated);
        $group->members()->sync($validated['members']);
        $group->members()->syncWithoutDetaching([$validated['leader_id']]);
        $newMembers = $group->members()->get()->map(function ($member) {
            return $member->id;
        })->toArray();
        
        // dd($oldMembers, $newMembers);
        foreach ($oldMembers as $memberId) {
            if (!in_array($memberId, $newMembers)) {
                User::find($memberId)->notify(new GroupNotification($group,  $request->user(), 'groupMemberRemoved'));
            }
        }

        foreach ($newMembers as $memberId) {
            if (!in_array($memberId, $oldMembers)) {
                User::find($memberId)->notify(new GroupNotification($group,  $request->user(), 'groupMemberAdded'));
            }
        }

        return redirect()->route('group.show', ['group' => $group->id])
            ->with('success', 'Group was edited!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, Group $group)
    {
        $members = $group->members;
        $group->delete();

        $members->each(function ($member) use ($request, $group) {
            if ($request->user()->id != $member->id) {
                $member->notify(new GroupNotification($group,  $request->user(), 'groupDeleted'));
            }
        });

        return redirect()->route('group.index')
            ->with('success', 'Group was deleted');
    }
}

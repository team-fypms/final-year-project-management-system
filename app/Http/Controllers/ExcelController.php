<?php

namespace App\Http\Controllers;

use App\Exports\LecturersExport;
use App\Exports\StudentsExport;
use App\Imports\LecturersImport;
use App\Imports\StudentsImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class ExcelController extends Controller
{
    public function exportStudent(){
        return Excel::download(new StudentsExport, 'students.xlsx');
    }

    public function exportLecturer(){
        return Excel::download(new LecturersExport, 'lecturers.xlsx');
    }

    public function importStudent(Request $request) 
    {
        $request->validate([
            'students' => 'required|file|mimes:csv,xlsx'
        ]);

        Excel::import(new StudentsImport, $request->file('students'));
        
        return redirect()->route('admin.student.index')->with('success', 'Students imported!');
    }

    public function importLecturer(Request $request) 
    {
        $request->validate([
            'lecturers' => 'required|file|mimes:csv,xlsx'
        ]);

        Excel::import(new LecturersImport, $request->file('lecturers'));
        
        return redirect()->route('admin.lecturer.index')->with('success', 'Lecturers imported!');
    }
}

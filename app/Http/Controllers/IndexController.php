<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Evaluation;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function uploadStudent()
    {
        return inertia('Admin/Student/Upload');
    }

    public function uploadLecturer()
    {
        return inertia('Admin/Lecturer/Upload');
    }

    public function dashboard(Request $request)
    {
        if ($request->user()->role == 'student') {
            $filters = $request->only('name', 'sortBy');
            $now = date("Y-m-d H:i:s");
            $evaluations = Evaluation::hydrate($request->user()->memberGroups()->with('evaluations')->get()->pluck('evaluations')->flatten()->toArray());

            return inertia('Index/Dashboard', [
                'evaluations' => $evaluations->count() ?
                    $evaluations
                    ->toQuery()
                    ->where('end_date_time', '>', $now)
                    ->filter($filters)
                    ->paginate(8)
                    ->withQueryString() : Evaluation::where('id', 0)->paginate()->withQueryString(),
                'filters' => $filters
            ]);
        } else if ($request->user()->role == 'lecturer') {
            $filters = $request->only('name', 'sortBy');
            $now = date("Y-m-d H:i:s");

            return inertia('Index/Dashboard', [
                'evaluations' => $request->user()->evaluations()
                    ->where('end_date_time', '>', $now)
                    ->filter($filters)
                    ->paginate(8)
                    ->withQueryString(),
                'filters' => $filters
            ]);
        } else {
            return redirect('/');
        }
    }
}

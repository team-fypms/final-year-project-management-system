<?php

namespace App\Http\Controllers;

use App\Models\Evaluation;
use Illuminate\Http\Request;
use App\Models\EvaluationFile;
use Illuminate\Support\Facades\Storage;

class AdminEvaluationFileController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Evaluation $evaluation)
    {
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Evaluation $evaluation, Request $request)
    {
        if ($request->hasFile('files')) {
            foreach ($request->file('files') as $file) {
                $path = $file->store('evaluations/' . $evaluation->id . '/files', 'public');

                $evaluation->files()->save(new EvaluationFile([
                    'file' => $path,
                    'name' => $file->getClientOriginalName(),
                    'ext' => $file->extension()
                ]));
            }
        }

        return redirect()->route('evaluation.show', ['evaluation' => $evaluation->id])->with('success', 'Files uploaded!');
    }

    /**
     * Display the specified resource.
     */
    public function show(EvaluationFile $evaluationFile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(EvaluationFile $evaluationFile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, EvaluationFile $evaluationFile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Evaluation $evaluation, EvaluationFile $evaluationFile)
    {
        Storage::disk('public')->delete($evaluationFile->file);
        $evaluationFile->delete();

        return redirect()->back()->with('success', 'File was deleted!');
    }
}

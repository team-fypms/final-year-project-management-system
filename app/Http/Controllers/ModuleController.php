<?php

namespace App\Http\Controllers;

use App\Models\Module;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ModuleController extends Controller
{
    function __construct()
    {
        $this->authorizeResource(Module::class, 'module');
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $filters = $request->only('name', 'sortBy');

        return inertia(
            'Module/Index',
            [
                'modules' => Module::filter($filters)
                    ->paginate(8)
                    ->withQueryString(),
                'filters' => $filters
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return inertia(
            'Module/Create'
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'code' => 'required',
            'year' => 'required|integer|min:1901|max:2155',
            'description' => 'required',
            'image' => 'nullable|image'
        ]);

        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('module_image', 'public');
            $validated['image'] = $path;
        }

        try {
            Module::create($validated);
        } catch (\Illuminate\Database\QueryException $exception) {
            if ($validated['image']) {
                Storage::disk('public')->delete($validated['image']);
            }
            throw $exception;
        }


        return redirect()->route('module.index')->with('success', 'Module created!');
    }

    /**
     * Display the specified resource.
     */
    public function show(Module $module)
    {
        $module->load('projects', 'projects.guide:id,name', 'projects.owner:id,name');

        return inertia(
            'Module/Show',
            [
                'module' => $module
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Module $module)
    {
        return inertia(
            'Module/Edit',
            [
                'module' => $module
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Module $module)
    {
        $validated = $request->validate([
            'name' => 'required',
            'code' => 'required',
            'year' => 'required|integer|min:1901|max:2155',
            'description' => 'required'
        ]);

        $module->update($validated);

        return redirect()->route('module.show', ['module' => $module->id])
            ->with('success', 'Module was edited');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Module $module)
    {
        $module->delete();

        return redirect()->route('module.index')
            ->with('success', 'Module was deleted');
    }
}

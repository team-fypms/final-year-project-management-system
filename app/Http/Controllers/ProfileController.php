<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ProfileController extends Controller
{
    function __construct()
    {
        $this->authorizeResource(User::class, 'user');
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, User $user)
    {
        $user->load('studentData', 'lecturerData');

        return inertia('Profile/Show', [
            'user' => $user,
            'canEdit' => $request->user()->id == $user->id
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user)
    {
        $user->load('studentData', 'lecturerData');

        return inertia('Profile/Edit', [
            'user' => $user,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user)
    {
        $currentUser = $request->user();

        if ($currentUser->role == 'admin') {
            $request->validate([
                'name' => 'required',
                'email' => ['required', 'email', Rule::unique('users')->ignore($user->email, 'email')],
            ]);

            $user->update($request->only('name', 'email'));
        } else if ($currentUser->role == 'student') {
            $request->validate([
                'name' => 'required',
                'email' => ['required', 'email', Rule::unique('users')->ignore($user->email, 'email')],
                'id' => ['required', Rule::unique('student_data')->ignore($user->studentData()->get()[0]->id)],
                'programme' => 'required',
                'admission_year' => 'required|integer|min:1901|max:2155'
            ]);

            $user->update($request->only('name', 'email'));
            $user->studentData()->update($request->only('id', 'programme', 'admission_year'));
        } else if ($currentUser->role == 'lecturer') {
            $request->validate([
                'name' => 'required',
                'email' => ['required', 'email', Rule::unique('users')->ignore($user->email, 'email')],
                'cid' => ['required', Rule::unique('lecturer_data')->ignore($user->lecturerData()->get()[0]->cid, 'cid')],
            ]);

            $user->update($request->only('name', 'email'));
            $user->lecturerData()->update($request->only('cid'));
        }

        return redirect()->route('profile.show', ['user' => $user->id])->with('success', 'User updated successfuly!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        //
    }
}

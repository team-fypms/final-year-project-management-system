<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationSeenAllController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $request->user()->unreadNotifications->markAsRead();

        if($request->user()->role == 'admin'){
            return redirect('/project')->with('success', 'All new notifications marked as read');
        }else{
            return redirect('/evaluation')->with('success', 'All new notifications marked as read');
        }
    }
}

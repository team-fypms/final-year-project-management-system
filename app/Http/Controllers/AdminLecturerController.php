<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\LecturerData;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AdminLecturerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $filters = $request->only('name', 'sortBy');

        return inertia(
            'Admin/Lecturer/Index',
            [
                'lecturers' => User::with('lecturerData')
                    ->where('role', 'lecturer')
                    ->filter($filters)
                    ->paginate(10)
                    ->withQueryString(),
                'filters' => $filters
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return inertia(
            'Admin/Lecturer/Create'
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:App\Models\User,email',
            'cid' => 'required|unique:App\Models\LecturerData,cid',
        ]);

        $user = User::create(['name' => $validated['name'], 'email' => $validated['email'], 'role' => 'lecturer', 'password' => $validated['cid']]);
        $user->lecturerData()->save(new LecturerData($validated));

        return redirect()->route('admin.lecturer.index')->with('success', 'Lecturer created successfuly!');
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user)
    {
        if ($user->role != 'lecturer') {
            return redirect()->back()->with('error', 'User is not a lecturer!');
        }

        $user->load(['lecturerData']);

        return inertia(
            'Admin/Lecturer/Edit',
            [
                'lecturer' => $user
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')->ignore($user->email, 'email')],
            'cid' => ['required', Rule::unique('lecturer_data')->ignore($user->lecturerData()->get()[0]->cid, 'cid')],
        ]);

        $user->update($request->only('name', 'email'));
        $user->lecturerData()->update($request->only('cid'));

        return redirect()->route('admin.lecturer.index')->with('success', 'Lecturer updated successfuly!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->back()->with('success', 'Lecturer deleted successfully!');
    }
}

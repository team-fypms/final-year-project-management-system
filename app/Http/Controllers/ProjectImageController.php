<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProjectImageController extends Controller
{
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, Project $project)
    {
        $validated = $request->validate([
            'image' => 'nullable|image'
        ]);

        
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('project_image', 'public');
            $validated['image'] = $path;
            if($project->image){
                Storage::disk('public')->delete($project->image);
            }
        }else{
            return redirect()->back()->with('success', 'Nothing was changed!');
        }

        $project->update($validated);

        return redirect()->route('project.show', ['project' => $project->id])
        ->with('success', 'Project image changed!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Project $project)
    {
        if($project->image){
            Storage::disk('public')->delete($project->image);
        }
        $project->image = null;
        $project->save();

        return redirect()->route('project.show', ['project' => $project->id])
        ->with('success', 'Project image removed!');
    }
}

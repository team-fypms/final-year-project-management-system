<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Models\ProjectGuideFile;
use Illuminate\Support\Facades\Storage;

class LecturerProjectGuideFileController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Project $project, Request $request)
    {
        if ($project->guide_id != $request->user()->id) {
            abort(403, 'Unauthorized');
        }

        if ($request->hasFile('files')) {
            foreach ($request->file('files') as $file) {
                $path = $file->store('projects/' . $project->id . '/guideFiles', 'public');

                $project->guideFiles()->save(new ProjectGuideFile([
                    'file' => $path,
                    'name' => $file->getClientOriginalName(),
                    'ext' => $file->extension()
                ]));
            }
        }

        return redirect()->back()->with('success', 'Guide files uploaded!');
    }

    /**
     * Display the specified resource.
     */
    public function show(ProjectGuideFile $projectGuideFile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ProjectGuideFile $projectGuideFile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ProjectGuideFile $projectGuideFile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, Project $project, ProjectGuideFile $projectGuideFile)
    {
        if ($project->guide_id != $request->user()->id) {
            abort(403, 'Unauthorized');
        }

        Storage::disk('public')->delete($projectGuideFile->file);
        $projectGuideFile->delete();

        return redirect()->back()->with('success', 'File was deleted!');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Models\ProjectOwnerFile;
use Illuminate\Support\Facades\Storage;

class LecturerProjectOwnerFileController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Project $project, Request $request)
    {
        if ($project->owner_id != $request->user()->id) {
            abort(403, 'Unauthorized');
        }

        if ($request->hasFile('files')) {
            foreach ($request->file('files') as $file) {
                $path = $file->store('projects/' . $project->id . '/ownerFiles', 'public');

                $project->ownerFiles()->save(new ProjectOwnerFile([
                    'file' => $path,
                    'name' => $file->getClientOriginalName(),
                    'ext' => $file->extension()
                ]));
            }
        }

        return redirect()->back()->with('success', 'Owner files uploaded!');
    }

    /**
     * Display the specified resource.
     */
    public function show(ProjectOwnerFile $projectOwnerFile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ProjectOwnerFile $projectOwnerFile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ProjectOwnerFile $projectOwnerFile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, Project $project, ProjectOwnerFile $projectOwnerFile)
    {
        if ($project->owner_id != $request->user()->id) {
            abort(403, 'Unauthorized');
        }

        Storage::disk('public')->delete($projectOwnerFile->file);
        $projectOwnerFile->delete();

        return redirect()->back()->with('success', 'File was deleted!');
    }
}

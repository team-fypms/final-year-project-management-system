<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Module;
use App\Models\Evaluation;
use Illuminate\Http\Request;
use App\Models\EvaluationFile;
use App\Models\Group;
use App\Notifications\EvaluationNotification;

class EvaluationController extends Controller
{
    function __construct()
    {
        $this->authorizeResource(Evaluation::class, 'evaluation');
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->user()->role == 'admin') {
            $filters = $request->only('name', 'sortBy');

            return inertia('Evaluation/Index', [
                'evaluations' => Evaluation::filter($filters)
                    ->paginate(8)
                    ->withQueryString(),
                'filters' => $filters
            ]);
        } else if ($request->user()->role == 'lecturer') {
            $filters = $request->only('name', 'sortBy');
            $ownerEvaluations = $request->user()->ownerProjects()->with('groups', 'groups.evaluations')->get()->pluck('evaluations')->flatten();
            $guideEvaluations = $request->user()->guideProjects()->with('groups', 'groups.evaluations')->get()->pluck('evaluations')->flatten();
            $evaluations = Evaluation::hydrate($ownerEvaluations->merge($guideEvaluations)->toArray());

            return inertia('Evaluation/Index', [
                'evaluations' => $evaluations->count() ?
                    $evaluations
                    ->toQuery()
                    ->filter($filters)
                    ->paginate(8)
                    ->withQueryString() : Evaluation::where('id', 0)->paginate()->withQueryString(),
                'filters' => $filters
            ]);
        } else {
            return redirect('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        $filters = $request->only(['evaluatorName']);

        return inertia('Evaluation/Create', [
            'modules' => Module::select('id', 'code', 'year', 'name')
                ->with('projects:id,module_id,name', 'projects.groups:id,project_id,name')
                ->get(),
            'evaluators' => User::findByNameOrEmail($request->evaluatorName, ['lecturer'])->select('id', 'name', 'email', 'role')->limit(20)->get(),
            'filters' => $filters
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'description' => 'required',
            'group_id' => 'required',
            'start_date_time' => 'required',
            'end_date_time' => 'required|after:start_date_time',
            'rubric' => 'required',
            'evaluators' => 'required|array|min:1|max:5',
        ]);

        $group = Group::find($validated['group_id']);
        $project = $group->project;
        if ($request->user()->role != 'admin' && $project->owner_id != $request->user()->id && $project->guide_id != $request->user()->id) {
            return back()->withErrors(['group_id' => 'Only guide & owners can create evaluation for their project groups']);
        }

        $evaluation = Evaluation::create($validated);
        $evaluation->evaluators()->sync($validated['evaluators']);
        if ($request->hasFile('files')) {
            foreach ($request->file('files') as $file) {
                $path = $file->store('evaluations/' . $evaluation->id . '/files', 'public');

                $evaluation->files()->save(new EvaluationFile([
                    'file' => $path,
                    'name' => $file->hashName(),
                    'ext' => $file->extension()
                ]));
            }
        }

        $evaluation->evaluators->each(function ($evaluator) use ($request, $evaluation) {
            if ($request->user()->id != $evaluator->id) {
                $evaluator->notify(new EvaluationNotification($evaluation, $request->user(), 'evaluationCreated'));
            }
        });

        $evaluation->group->members->each(function ($member) use ($request, $evaluation) {
            if ($request->user()->id != $member->id) {
                $member->notify(new EvaluationNotification($evaluation, $request->user(), 'evaluationCreated'));
            }
        });

        return redirect()->route('evaluation.index')->with('success', 'Evaluation created!');
    }

    /**
     * Display the specified resource.
     */
    public function show(Evaluation $evaluation)
    {
        $evaluation->load('group', 'group.members:id', 'evaluators', 'files', 'submissions');

        return inertia('Evaluation/Show', [
            'evaluation' => $evaluation
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, Evaluation $evaluation)
    {
        $evaluation->load('evaluators:id,name,email,role', 'group:id,project_id', 'group.project:id,module_id', 'files', 'submissions');
        $filters = $request->only(['evaluatorName']);

        return inertia('Evaluation/Edit', [
            'modules' => Module::select('id', 'code', 'year', 'name')
                ->with('projects:id,module_id,name', 'projects.groups:id,project_id,name')
                ->get(),
            'evaluators' => User::findByNameOrEmail($request->evaluatorName, ['lecturer'])->select('id', 'name', 'email', 'role')->limit(20)->get(),
            'filters' => $filters,
            'evaluation' => $evaluation
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Evaluation $evaluation)
    {
        $validated = $request->validate([
            'name' => 'required',
            'description' => 'required',
            'group_id' => 'required',
            'start_date_time' => 'required',
            'end_date_time' => 'required|after:start_date_time',
            'rubric' => 'required',
            'evaluators' => 'required|array|min:1|max:5',
        ]);

        $project = $evaluation->group->project;
        if ($request->user()->role != 'admin' && $project->owner_id != $request->user()->id && $project->guide_id != $request->user()->id) {
            return back()->withErrors(['group_id' => 'Only guide & owners can update evaluation for their project groups']);
        }

        $evaluation->update($validated);
        $evaluation->evaluators()->sync($validated['evaluators']);

        return redirect()->route('evaluation.show', ['evaluation' => $evaluation->id])
            ->with('success', 'Evaluation created!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, Evaluation $evaluation)
    {
        $project = $evaluation->group->project;
        if ($request->user()->role != 'admin' && $project->owner_id != $request->user()->id && $project->guide_id != $request->user()->id) {
            return back()->withErrors(['group_id' => 'Only guide & owners can update evaluation for their project groups']);
        }

        $evaluators = $evaluation->evaluators;
        $members = $evaluation->group->members;
        $evaluation->delete();
        $evaluators->each(function ($evaluator) use ($request, $evaluation) {
            if ($request->user()->id != $evaluator->id) {
                $evaluator->notify(new EvaluationNotification($evaluation, $request->user(), 'evaluationDeleted'));
            }
        });
        $members->each(function ($member) use ($request, $evaluation) {
            if ($request->user()->id != $member->id) {
                $member->notify(new EvaluationNotification($evaluation, $request->user(), 'evaluationDeleted'));
            }
        });

        return redirect()->route('evaluation.index')
            ->with('success', 'Evaluation was deleted');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Group;
use Illuminate\Http\Request;
use App\Notifications\GroupNotification;

class GroupLeaveController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request, Group $group)
    {
        if ($group->leader_id == $request->user()->id) {
            abort(403, 'Leader cannot leave the group');
        }

        $group->members()->detach($request->user()->id);
        $group->members->each(function ($member) use($request, $group) {
            $member->notify(new GroupNotification($group,  $request->user(), 'groupMemberLeft'));
        });

        return redirect()->back()->with('success', 'You left the group!');
    }
}

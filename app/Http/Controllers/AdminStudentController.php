<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\StudentData;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AdminStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $filters = $request->only('name', 'programme', 'admission_year');
        return inertia(
            'Admin/Student/Index',
            [
                'students' => User::with('studentData')
                    ->where('role', 'student')
                    ->studentFilter($filters)
                    ->paginate(10)
                    ->withQueryString(),
                'filters' => $filters
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return inertia(
            'Admin/Student/Create'
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:App\Models\User,email',
            'id' => 'required|unique:App\Models\StudentData,id',
            'programme' => 'required',
            'admission_year' => 'required|integer|min:1901|max:2155'
        ]);

        $user = User::create(['name' => $validated['name'], 'email' => $validated['email'], 'role' => 'student', 'password' => $validated['id']]);
        $user->studentData()->save(new StudentData($validated));

        return redirect()->route('admin.student.index')->with('success', 'Student created successfuly!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user)
    {
        if ($user->role != 'student') {
            return redirect()->back()->with('error', 'User is not student!');
        }

        $user->load(['studentData']);

        return inertia(
            'Admin/Student/Edit',
            [
                'student' => $user
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')->ignore($user->email, 'email')],
            'id' => ['required', Rule::unique('student_data')->ignore($user->studentData()->get()[0]->id)],
            'programme' => 'required',
            'admission_year' => 'required|integer|min:1901|max:2155'
        ]);

        $user->update($request->only('name', 'email'));
        $user->studentData()->update($request->only('id', 'programme', 'admission_year'));

        return redirect()->route('admin.student.index')->with('success', 'Student updated successfuly!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->back()->with('success', 'Student deleted successfully!');
    }
}

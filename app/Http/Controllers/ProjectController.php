<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\User;
use App\Models\Module;
use App\Notifications\ProjectNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProjectController extends Controller
{
    function __construct()
    {
        $this->authorizeResource(Project::class, 'project');
    }

    public function index(Request $request)
    {
        if ($request->user()->role == 'admin') {
            $filters = $request->only('name', 'sortBy');

            return inertia(
                'Project/Index',
                [
                    'projects' => Project::join('modules', 'projects.module_id', '=', 'modules.id')
                        ->select('projects.*', 'modules.year as module_year', 'modules.name as module_name')
                        ->with('owner:id,name')
                        ->with('guide:id,name')
                        ->filter($filters)
                        ->paginate(8)
                        ->withQueryString(),
                    'filters' => $filters
                ]
            );
        } else if ($request->user()->role == 'student') {
            $filters = $request->only('name', 'sortBy');
            $projects = Project::hydrate($request->user()->memberGroups()->with('project')->get()->pluck('project')->flatten()->toArray());

            return inertia(
                'Project/Index',
                [
                    'projects' => $projects->count() ?
                        $projects
                        ->toQuery()
                        ->join('modules', 'projects.module_id', '=', 'modules.id')
                        ->select('projects.*', 'modules.year as module_year', 'modules.name as module_name')
                        ->with('owner:id,name')
                        ->with('guide:id,name')
                        ->filter($filters)
                        ->paginate(8)
                        ->withQueryString() : Project::where('id', 0)->paginate()->withQueryString(),
                    'filters' => $filters
                ]
            );
        } else if ($request->user()->role == 'lecturer') {
            $filters = $request->only('name', 'sortBy');
            $projects = Project::hydrate($request->user()->ownerProjects()->union($request->user()->guideProjects())->get()->toArray());

            return inertia(
                'Project/Index',
                [
                    'projects' => $projects->count() ?
                        $projects
                        ->toQuery()
                        ->join('modules', 'projects.module_id', '=', 'modules.id')
                        ->select('projects.*', 'modules.year as module_year', 'modules.name as module_name')
                        ->with('owner:id,name')
                        ->with('guide:id,name')
                        ->filter($filters)
                        ->paginate(8)
                        ->withQueryString() : Project::where('id', 0)->paginate(),
                    'filters' => $filters
                ]
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        $filters = $request->only(['guideName', 'ownerName']);

        return inertia(
            'Project/Create',
            [
                'modules' => Module::select('name', 'year', 'code', 'id')->get(),
                'guides' => User::findByNameOrEmail($request->guideName, ['lecturer'])->select('id', 'name', 'email', 'role')->get(),
                'owners' => User::findByNameOrEmail($request->ownerName, ['lecturer'])->select('id', 'name', 'email', 'role')->get(),
                'filters' => $filters
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'module' => 'required',
            'description' => 'required',
            'owner' => 'required',
            'guide' => 'required',
            'image' => 'nullable|image'
        ]);
        $validated['module_id'] = $request['module'];
        $validated['owner_id'] = $request['owner'];
        $validated['guide_id'] = $request['guide'];


        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('project_image', 'public');
            $validated['image'] = $path;
        }

        try {
            $project = Project::create($validated);

            $project->owner->notify(new ProjectNotification($project, $request->user(), 'projectCreatedOwner'));
            $project->guide->notify(new ProjectNotification($project, $request->user(), 'projectCreatedGuide'));
        } catch (\Illuminate\Database\QueryException $exception) {
            if ($validated['image']) {
                Storage::disk('public')->delete($validated['image']);
            }
            throw $exception;
        }

        return redirect()->route('project.index')->with('success', 'Project created!');
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, Project $project)
    {
        $project->load('module:id,name,code,year', 'owner:id,name,role,email', 'guide:id,name,role,email', 'groups', 'ownerFiles', 'guideFiles');

        return inertia(
            'Project/Show',
            [
                'project' => $project
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, Project $project)
    {
        $filters = $request->only(['guideName', 'ownerName']);

        return inertia(
            'Project/Edit',
            [
                'project' => $project,
                'modules' => Module::select('name', 'year', 'code', 'id')->get(),
                'guides' => User::findByNameOrEmail($request->guideName, ['lecturer'])->select('id', 'name', 'email', 'role')->get(),
                'owners' => User::findByNameOrEmail($request->ownerName, ['lecturer'])->select('id', 'name', 'email', 'role')->get(),
                'filters' => $filters,
                'owner' => User::find($project->owner_id),
                'guide' => User::find($project->guide_id)
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Project $project)
    {
        $validated = $request->validate([
            'name' => 'required',
            'module' => 'required',
            'description' => 'required',
            'owner' => 'required',
            'guide' => 'required'
        ]);
        $validated['module_id'] = $request['module'];
        $validated['owner_id'] = $request['owner'];
        $validated['guide_id'] = $request['guide'];

        $oldOwner = User::find($project->owner_id);
        $oldGuide = User::find($project->guide_id);
        $oldModuleId = $project->module_id;
        $project->update($validated);
        if ($validated['owner_id'] != $oldOwner->id) {
            $oldOwner->notify(new ProjectNotification($project, $request->user(), 'projectOwnerRemoved'));
            $project->owner->notify(new ProjectNotification($project, $request->user(), 'projectOwnerAdded'));
        }
        if ($validated['guide_id'] != $oldGuide->id) {
            $oldGuide->notify(new ProjectNotification($project, $request->user(), 'projectGuideRemoved'));
            $project->guide->notify(new ProjectNotification($project, $request->user(), 'projectGuideAdded'));
        }
        if ($validated['module_id'] != $oldModuleId) {
            $project->guide->notify(new ProjectNotification($project, $request->user(), 'projectModuleChanged'));
            if ($project->owner_id != $project->guide_id) {
                $project->owner->notify(new ProjectNotification($project, $request->user(), 'projectModuleChanged'));
            }
        }

        return redirect()->route('project.show', ['project' => $project->id])
            ->with('success', 'Project was edited');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, Project $project)
    {
        $project->delete();
        $project->owner->notify(new ProjectNotification($project, $request->user(), 'projectDeleted'));
        if ($project->owner_id != $project->guide_id) {
            $project->guide->notify(new ProjectNotification($project, $request->user(), 'projectDeleted'));
        }
        return redirect()->route('project.index')
            ->with('success', 'Project was deleted');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserFinder extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        return User::findByNameOrEmail($request->value, $request->roles)->select('name', 'email');
    }
}

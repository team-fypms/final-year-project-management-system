<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Auth\EmailVerificationRequest

class AuthenticateWithRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, string $role): Response
    {
        if (!$request->user()){
            return redirect('/login')->with('error','Please login!');
        }
        else if ($request->user()->role !== $role) {
            if ($request->user()->role === 'admin'){
                return redirect('admin/project')->withErrors(['error' => 'Permission Denied!']);
            }else if($request->user()->role === 'student'){
                return redirect('student/dashboard')->withErrors(['error' => 'Permission Denied!']);
            }else{
                return redirect('lecturer/dashboard')->withErrors(['error' => 'Permission Denied!']);
            }
        }
 
        return $next($request);
    }
}

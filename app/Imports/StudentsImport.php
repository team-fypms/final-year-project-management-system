<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class StudentsImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $userCount = User::join('student_data', 'users.id', '=', 'student_data.user_id')
                ->select('users.*', 'student_data.*')
                ->where('student_data.id', $row['student_id'])
                ->orWhere('users.email', $row['student_id'] . '.gcit@rub.edu.bt')
                ->count();

            if ($userCount == 0) {
                User::create([
                    'name'     => $row['name'],
                    'email'    => $row['student_id'] . '.gcit@rub.edu.bt',
                    'role' => 'student',
                    'password' => Hash::make($row['student_id'])
                ])->studentData()->create([
                    'id' => $row['student_id'],
                    'programme' => $row['programme'],
                    'admission_year' => $row['admission_year']
                ]);
            }
        }
    }
}

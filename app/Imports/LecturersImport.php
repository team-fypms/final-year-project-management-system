<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class LecturersImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $userCount = User::join('lecturer_data', 'users.id', '=', 'lecturer_data.user_id')
                ->select('users.*', 'lecturer_data.*')
                ->where('lecturer_data.cid', $row['cid'])
                ->orWhere('users.email', $row['email'])
                ->count();

            if ($userCount == 0 && ($row['position_title'] == 'Assistant Lecturer' || $row['position_title'] == 'Associate Lecturer')) {
                User::create([
                    'name'     => $row['name'],
                    'email'    => $row['email'],
                    'role' => 'lecturer',
                    'password' => Hash::make($row['cid'])
                ])->lecturerData()->create([
                    'cid' => $row['cid'],
                ]);
            }
        }
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Group extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'project_id', 'leader_id'];

    public function leader(): BelongsTo
    {
        return $this->belongsTo(User::class, 'leader_id');
    }

    public function members(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class);
    }

    public function evaluations(): HasMany
    {
        return $this->hasMany(Evaluation::class);
    }

    public function scopeFilter(Builder $query, array $filters): Builder
    {
        return $query
            ->when(
                $filters['name'] ?? false,
                fn ($query, $value) => $query->where('name', 'like', '%' . $value . '%')
            )
            ->when(
                $filters['sortBy'] ?? false,
                fn ($query, $value) => !in_array($value, ['latest', 'oldest']) ?
                    $query->latest() : ($value == 'latest' ? $query->latest() : $query->oldest()),
                fn ($query, $value) => $query->latest()
            );
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($group) {
            $group->evaluations->each(function ($evaluation) {
                $evaluation->delete();
            });
        });
    }
}

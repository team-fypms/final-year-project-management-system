<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class LecturerData extends Model
{
    use HasFactory;

    protected $fillable = ['cid'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(
            \App\Models\User::class
        );
    }
}

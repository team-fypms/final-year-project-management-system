<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'role',
        'password'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    protected $student_courses = ['Bachelor of Science in Computer Science', 'Bachelor of Science in Information Technology', 'Bachelor Of Computer Science (AI Development & Data Science)', 'Bachelor Of Computer Science (Blockchain Development)', 'Bachelor Of Computer Science (Full Stack Development)', 'Bachelor of Digital Media & Development'];

    public function studentData(): HasOne
    {
        return $this->hasOne(StudentData::class);
    }

    public function lecturerData(): HasOne
    {
        return $this->hasOne(LecturerData::class);
    }

    public function ownerProjects(): HasMany
    {
        return $this->hasMany(Project::class, 'owner_id');
    }

    public function guideProjects(): HasMany
    {
        return $this->hasMany(Project::class, 'guide_id');
    }

    public function memberGroups(): BelongsToMany
    {
        return $this->belongsToMany(Group::class);
    }

    public function evaluations(): BelongsToMany
    {
        return $this->belongsToMany(Evaluation::class);
    }

    public function leaderGroups(): HasMany
    {
        return $this->hasMany(Group::class, 'leader_id');
    }

    // public function myEvaluations(): HasManyThrough
    // {
    //     return $this->hasManyThrough(Evaluation::class, Group::class);
    // }

    // public static function boot()
    // {
    //     parent::boot();
    //     self::deleting(function ($user) {
    //         $user->studentData()->delete();
    //     });
    // }
    public function scopeFilter(Builder $query, array $filters): Builder
    {
        return $query
            ->when(
                $filters['name'] ?? false,
                fn ($query, $value) => $query->where('name', 'like', '%' . $value . '%')
            )
            ->when(
                $filters['sortBy'] ?? false,
                fn ($query, $value) => !in_array($value, ['latest', 'oldest']) ?
                    $query->latest() : ($value == 'latest' ? $query->latest() : $query->oldest()),
                fn ($query, $value) => $query->latest()
            );
    }

    public function scopeStudentFilter(Builder $query, array $filters): Builder
    {
        return $query
            ->when(
                $filters['name'] ?? false,
                fn ($query, $value) => $query->where('name', 'like', '%' . $value . '%')
            )
            ->when(
                $filters['programme'] ?? false,
                fn ($query, $value) => !in_array($value, $this->student_courses) ?
                    $query : $query->whereHas('studentData', function (Builder $query) use ($value) {
                        $query->where('programme', $value);
                    })
            )
            ->when(
                $filters['admission_year'] ?? false,
                fn ($query, $value) => $query->whereHas('studentData', function (Builder $query) use ($value) {
                    $query->where('admission_year', $value);
                })
            );
    }

    public function scopeFindByNameOrEmail(Builder $query, $value, array $roles): Builder
    {
        return $query
            ->when(
                $roles ?? false,
                fn ($query, $roles) => $query->whereIn('role', $roles)
            )
            ->where(
                function (Builder $query) use ($value) {
                    $query->when(
                        $value ?? false,
                        fn ($query, $value) => $query->where('name', 'like', '%' . $value . '%')->orWhere('email', 'like', '%' . $value . '%')
                    );
                }
            );
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Project extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description', 'image', 'module_id', 'owner_id', 'guide_id'];
    protected $appends = ['image_src'];
    protected $sorts = ['latest', 'oldest'];

    public function getImageSrcAttribute()
    {
        return asset("storage/{$this->image}");
    }

    public function module(): BelongsTo
    {
        return $this->belongsTo(Module::class);
    }

    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function guide(): BelongsTo
    {
        return $this->belongsTo(User::class, 'guide_id');
    }

    public function groups(): HasMany
    {
        return $this->hasMany(Group::class);
    }

    public function guideFiles(): HasMany
    {
        return $this->hasMany(ProjectGuideFile::class);
    }

    public function ownerFiles(): HasMany
    {
        return $this->hasMany(ProjectOwnerFile::class);
    }

    public function evaluations(): HasManyThrough
    {
        return $this->hasManyThrough(Evaluation::class, Group::class);
    }

    public function scopeFilter(Builder $query, array $filters): Builder
    {


        return $query
            ->when(
                $filters['name'] ?? false,
                fn ($query, $value) => $query->where('projects.name', 'like', '%' . $value . '%')
            )
            ->when(
                $filters['sortBy'] ?? false,
                fn ($query, $value) => !in_array($value, $this->sorts) ?
                    $query->latest() : ($value == 'latest' ? $query->orderByDesc('module_year') : $query->orderBy('module_year')),
                fn ($query, $value) => $query->orderByDesc('module_year')
            );
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($project) {
            if ($project->image) {
                Storage::disk('public')->delete($project->image);
            }
            Storage::disk('public')->deleteDirectory('projects/' . $project->id);

            $project->groups->each(function($group){
                $group->delete();
            });
        });
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class StudentData extends Model
{
    use HasFactory;

    protected $fillable = ['id', 'programme', 'admission_year'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(
            \App\Models\User::class
        );
    }
}

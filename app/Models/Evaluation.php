<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Evaluation extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description', 'group_id', 'start_date_time', 'end_date_time', 'rubric'];
    protected $sorts = ['latest', 'oldest'];

    public function group(): BelongsTo
    {
        return $this->belongsTo(Group::class);
    }

    public function evaluators(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function files(): HasMany
    {
        return $this->hasMany(EvaluationFile::class);
    }

    public function submissions(): HasMany
    {
        return $this->hasMany(EvaluationSubmission::class);
    }

    public function scopeFilter(Builder $query, array $filters): Builder
    {
        return $query
            ->when(
                $filters['name'] ?? false,
                fn ($query, $value) => $query->where('name', 'like', '%' . $value . '%')
            )
            ->when(
                $filters['sortBy'] ?? false,
                fn ($query, $value) => !in_array($value, $this->sorts) ?
                    $query->latest() : ($value == 'latest' ? $query->latest() : $query->oldest()),
                fn ($query, $value) => $query->latest()
            );
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($evaluation) {
            Storage::disk('public')->deleteDirectory('evaluations/' . $evaluation->id);
        });
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Storage;

class Module extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'code', 'year', 'description', 'image'];
    protected $appends = ['image_src'];
    protected $sorts = ['latest', 'oldest'];

    public function getImageSrcAttribute()
    {
        return asset("storage/{$this->image}");
    }

    public function projects(): HasMany
    {
        return $this->hasMany(\App\Models\Project::class);
    }

    public function scopeFilter(Builder $query, array $filters): Builder
    {
        return $query
            ->when(
                $filters['name'] ?? false,
                fn ($query, $value) => $query->where('name', 'like', '%' . $value . '%')
            )
            ->when(
                $filters['sortBy'] ?? false,
                fn ($query, $value) => !in_array($value, $this->sorts) ?
                    $query->latest() : ($value == 'latest' ? $query->orderByDesc('year') : $query->orderBy('year')),
                fn ($query, $value) => $query->orderByDesc('year')
            );
    }

    public static function boot()
    {
        parent::boot();

        self::deleting(function ($module) {
            if ($module->image) {
                Storage::disk('public')->delete($module->image);
            }

            $module->projects->each(function ($project) {
                $project->delete();
            });
        });
    }
}

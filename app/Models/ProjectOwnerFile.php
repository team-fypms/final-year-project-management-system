<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProjectOwnerFile extends Model
{
    use HasFactory;

    protected $fillable = ['file', 'name', 'ext'];
    protected $appends = ['file_url'];


    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class);
    }

    public function getFileUrlAttribute()
    {
        return asset("storage/{$this->file}");
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($project_owner_file) {
            Storage::disk('public')->delete($project_owner_file->file);
        });
    }
}

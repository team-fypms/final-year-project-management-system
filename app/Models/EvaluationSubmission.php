<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EvaluationSubmission extends Model
{
    use HasFactory;

    protected $fillable = ['file', 'name', 'ext'];
    protected $appends = ['file_url'];

    public function evaluation(): BelongsTo
    {
        return $this->belongsTo(Evaluation::class);
    }

    public function getFileUrlAttribute()
    {
        return asset("storage/{$this->file}");
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($evaluation_submission) {
            Storage::disk('public')->delete($evaluation_submission->file);
        });
    }
}

<?php

namespace App\Notifications;

use App\Models\User;
use App\Models\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ProjectNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(
        private Project $project,
        private User $user,
        private $type
    ) {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        if ($this->type === 'projectCreatedOwner') {
            return (new MailMessage)
                ->line("Project {$this->project->name} created! You were added as the the owner.")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        } else if ($this->type === 'projectCreatedGuide') {
            return (new MailMessage)
                ->line("Project {$this->project->name} created! You were added as the the guide.")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        } else if ($this->type === 'projectDeleted') {
            return (new MailMessage)
                ->line("Project {$this->project->name} deleted!")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        } else if ($this->type === 'projectOwnerRemoved') {
            return (new MailMessage)
                ->line("You were removed as the owner of the project {$this->project->name}!")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        } else if ($this->type === 'projectOwnerAdded') {
            return (new MailMessage)
                ->line("You were added as the owner of the project {$this->project->name}!")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        } else if ($this->type === 'projectGuideRemoved') {
            return (new MailMessage)
                ->line("You were removed as the guide of the project {$this->project->name}!")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        } else if ($this->type === 'projectGuideAdded') {
            return (new MailMessage)
                ->line("You were added as the guide of the project {$this->project->name}!")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        } else if ($this->type === 'projectModuleChanged') {
            return (new MailMessage)
                ->line("Project {$this->project->name}'s module changed!")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            'project_id' => $this->project->id,
            'project_name' => $this->project->name,
            'module_id' => $this->project->module_id,
            'type' => $this->type,
            'user_id' => $this->user->id
        ];
    }
}

<?php

namespace App\Notifications;

use App\Models\Group;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Auth\User;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class GroupNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(
        private Group $group,
        private User $user,
        private $type,
    ) {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        if ($this->type === 'groupCreated') {
            return (new MailMessage)
                ->line("Group {$this->group->name} created!")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        } else if ($this->type === 'groupDeleted') {
            return (new MailMessage)
                ->line("Group {$this->group->name} deleted!")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        } else if ($this->type === 'groupMemberLeft') {
            return (new MailMessage)
                ->line("Someone left the group {$this->group->name}!")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        } else if ($this->type === 'groupMemberRemoved') {
            return (new MailMessage)
                ->line("You were removed from the group {$this->group->name}!")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        } else if ($this->type === 'groupMemberAdded') {
            return (new MailMessage)
                ->line("You were added in the group {$this->group->name}!")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        } else if ($this->type === 'groupLeaderChanged') {
            return (new MailMessage)
                ->line("Group {$this->group->name}'s leader changed!")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        } else if ($this->type === 'groupLeaderRemoved') {
            return (new MailMessage)
                ->line("You were removed as the leader from the group {$this->group->name}!")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        } else if ($this->type === 'groupLeaderAdded') {
            return (new MailMessage)
                ->line("You were made as the leader for the group {$this->group->name}!")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            'group_id' => $this->group->id,
            'group_name' => $this->group->name,
            'project_id' => $this->group->project_id,
            'type' => $this->type,
            'user_id' => $this->user->id
        ];
    }
}

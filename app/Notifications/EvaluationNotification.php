<?php

namespace App\Notifications;

use App\Models\User;
use App\Models\Evaluation;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class EvaluationNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(
        private Evaluation $evaluation,
        private User $user,
        private $type
    ) {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        if ($this->type === 'evaluationCreated') {
            return (new MailMessage)
                ->line("An evaluation {$this->evaluation->name} for group {$this->evaluation->group->name} created!")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        } else if ($this->type === 'evaluationDeleted') {
            return (new MailMessage)
                ->line("Evaluation {$this->evaluation->name} deleted!")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        } else if ($this->type === 'evaluationSubmissionsAdded') {
            return (new MailMessage)
                ->line("Group {$this->evaluation->group->name} has submitted one or more submission files for evaluation {$this->evaluation->name}!")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        } else if ($this->type === 'evaluationSubmissionsDeleted') {
            return (new MailMessage)
                ->line("Group {$this->evaluation->group->name} has deleted a submission file for evaluation {$this->evaluation->name}!")
                ->action('Checkout', url('/notification'))
                ->line('Thank you for using our application!');
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            'evaluation_id' => $this->evaluation->id,
            'evaluation_name' => $this->evaluation->name,
            'group_id' => $this->evaluation->group_id,
            'type' => $this->type,
            'user_id' => $this->user->id
        ];
    }
}

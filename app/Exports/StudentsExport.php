<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class StudentsExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'name',
            'email',
            'student_id',
            'programme',
            'admission_year'
        ];
    }

    public function collection()
    {
        return User::select('name', 'email', 'id')->with('studentData')->where('role', 'student')->get()->each(function ($user) {
            $student_data = $user->studentData;
            unset($user['id']);
            $user->student_id = $student_data->id;
            $user->programme = $student_data->programme;
            $user->admission_year = $student_data->admission_year;
        });
    }
}

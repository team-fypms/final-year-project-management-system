<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class LecturersExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'name',
            'email',
            'cid',
        ];
    }

    public function collection()
    {
        return User::select('name', 'email', 'id')->with('lecturerData')->where('role', 'lecturer')->get()->each(function ($user) {
            $lecturer_data = $user->lecturerData;
            unset($user['id']);
            $user->cid = $lecturer_data->cid;
        });
    }
}
